/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require("fs");
//created deleteFiles function which return a promise for delele a file operation
function deleteFiles(filename) {
	return new Promise(function (resolve, reject) {
		fs.unlink(filename, function (err) {
			if (err) {
				reject(err);
			} else {
				resolve(filename);
			}
		});
	});
}
//created read file function which return a promise for read file opeation
function readFile(filename, encoding) {
	return new Promise(function (resolve, reject) {
		console.log(`reading data from file ${filename}`);
		fs.readFile(filename, encoding, function (err, data) {
			if (err) {
				reject(err);
			} else {
				console.log(`get the data from file ${filename}`);
				resolve(data);
			}
		});
	});
}
//created write file function which return a promise for write file operation
function writeFile(filename, data) {
	return new Promise(function (resolve, reject) {
		console.log(`writing data to file ${filename}`);
		fs.writeFile(filename, data, function (err) {
			if (err) {
				reject(err);
			} else {
				console.log(`write data sucessfully in ${filename}`);
				resolve(filename);
			}
		});
	});
}
//crated append file function which return a promise for append file operation
function appendFile(filename, data) {
	return new Promise(function (resolve, reject) {
		console.log(`appending data to a file${filename}`);
		fs.appendFile(filename, data + "\n", function (err) {
			if (err) {
				reject(err);
			} else {
				console.log(`append data sucessfully in ${filename}`);
				resolve(filename);
			}
		});
	});
}
function main() {
	readFile("lipsum.txt", "utf-8")
		.then(function (data) {
			data = data.toString().toUpperCase();
			writeFile("upperContent.txt", data).then(function (filename) {
				appendFile("filenames.txt", filename);
				readFile(filename, "utf-8").then(function (data) {
					data = data.toString().toLowerCase().split(".").join("\r\n");
					writeFile("lowerContent.txt", data).then(function (filename) {
						appendFile("filenames.txt", filename);
						readFile(filename, "utf-8").then(function (data) {
							data = data
								.split("\n")
								.sort((list1, list2) => list1.localeCompare(list2))
								.join("\r\n");
							writeFile("sortedContent.txt", data).then(function (filename) {
								appendFile("filenames.txt", filename).then(function (filename) {
									readFile(filename, "utf-8").then(function (data) {
										data = data.split("\n");
										for (let file = 0; file < data.length - 1; file++) {
											if (data[file] !== "") {
												deleteFiles(data[file]).then(function (filename) {
													console.log("Files deleted");
												});
											}
										}
									});
								});
							});
						});
					});
				});
			});
		})
		.catch(function (err) {
			console.log(err);
		});
}
module.exports = { main, writeFile, readFile, appendFile, deleteFiles };
