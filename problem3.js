/*
 Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs = require("fs");
//created deleteFiles function which return a promise for delele a file operation
function deleteFiles(filename) {
	return new Promise(function (resolve, reject) {
		fs.unlink(filename, function (err) {
			if (err) {
				reject(err);
			} else {
				resolve(filename);
			}
		});
	});
}
//created writefiles function which return a promise for writing a file operation
function writeFiles(filename, data) {
	return new Promise(function (resolve, reject) {
		fs.writeFile(filename, data, function (err) {
			if (err) {
				reject(err);
			} else {
				console.log(`Write to file ${filename}`);
				resolve(filename);
			}
		});
	});
}
//main function which execute opeation line by line
function main() {
	//genearate a random number
	const randomNumber = Math.floor(Math.random() * 5 + 1); //
	//create promise list
	let promiseList = [];
	for (let file = 1; file < randomNumber; file++) {
		promiseList.push(writeFiles(`file${file}.txt`, '{name:"xxx",age:"33"}'));
	}
	//if all the files are creted than it will return resolved
	Promise.all(promiseList)
		.then(function () {
			for (let file = 1; file < randomNumber; file++) {
				deleteFiles(`file${file}.txt`).then(function (filename) {
					console.log(`deleted file ${filename}`);
				});
			}
		})
		.catch(function (err) {
			console.log("Error in deleting", err);
		});
}
main();
